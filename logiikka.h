#ifndef LOGIIKKA_H
#define LOGIIKKA_H
#include <QImage>
#include <memory>

class Logiikka
{
public:
    Logiikka();
    void muunnaKaistale(const QImage& kuva, int vasen, int oikea,
                        std::vector<QColor>& vekki);
    void muunnaPala(const QImage &kuva, int alkux, int alkuy,
                    int leveys, int korkeus, std::vector<QColor>& vekki);
private:

};

#endif // LOGIIKKA_H
