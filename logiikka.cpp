#include "logiikka.h"
#include <QLabel>
#include <memory>
#include <iostream>
#include <thread>

Logiikka::Logiikka()
{
    std::cout << "logiikka luotu; muista alustaa muuttujat yms." << std::endl;
    // pääikkuna luo Logiikka-tyyppisen olion ohjelman suorituksen alkaessa
    // tämä on hyvä hetki luoda logiikalle tarvittavat privaatit muuttujat
    // ja esim. laittaa säikeet nukkumaan ja odottamaan pääikkunan puolelta tulevia
    // jäsenfunktiokutsuja muunnaKaistaleelle
}

void Logiikka::muunnaKaistale(const QImage& kuva, int vasen, int oikea, std::vector<QColor>& tallennusVektori)
{

    // parametreina saatu kuva, kaistaleen vasemman laidan etäisyys kuvan vasemmasta laidasta
    // ja kaistaleen oikean laidan etäisyys vasemmasta laidasta ja vectori, jonne talletetaan
    // muunnetut pikselit

    // tämä yksisäikeinen koodi jakaa kaistaleen 100 pikselin korkuisiin paloihin
    // kaistaleesta käsitellään silmukassa kukin pala kerrallaan aloittaen ylimmästä palasta
    // käytännössä esimerkin 600x600 px kuvalla muodostuu siis 6 kpl 100x100 px palasia,
    // jotka kaikki muunnetaan erikseen

    int palaMaara = kuva.height() / 100;

    // paikallinen vektori, jonka sisällä on vektoreita
    // ulommassa vektorissa on niin monta paikkaa kuin kaistaleessa paloja
    // jokainen vektorin alkio on tyhjä vektori
    // jokainen säie käsittelee oman palansa ja tallettaa muunnetut pikselit
    // säikeelle osoitettuun vektoriin, joka löytyy ulomman vektorin
    // tietystä indeksistä

    // tämä siis kuvaa kaistaleen pikselit paloina; palat itsessään vektoreita,
    // joiden sisällä pikseleitä
    std::vector<std::vector<QColor>> palaVektori(palaMaara);

    // TÄSSÄ KOHDASSA SÄIKEET RYHTYVÄT TÖIHIN JA MUUNTAVAT OMAN PALANSA


    for (int i = 0; i < palaMaara; i += 1) {

        // tietyn palan muunnokset talletetaan tähän juuri tuolle palalle
        // tarkoitettuun vektoriin
        std::vector<QColor>& saikeenPalaVektori = palaVektori[i];

        // tämän työn tekee rinnakkaisessa ohjelmassa yksi säie muista säikeistä
        // riippumatta ja samanaikaisesti
        this->muunnaPala(kuva, vasen, i*100, oikea - vasen, 100, saikeenPalaVektori);

        std::cout << "pala " << i << " ok" << std::endl;
    }

    // TÄSTÄ ALKAA KERÄYSVAIHE – VIIMEINEN SÄIE SAAPUI BARRIERILLE
    // säikeet ovat nyt nukkumassa ja odottamassa seuraavaa muunnaKaistale-kutsua


    // nyt kaikki palat on muunnettu ja tulokset on talletettu pala palalta
    // palaVektoriin

    // käydään palaVektori läpi paloittain ja lisätään jokaisen palan jokainen pikseli
    // tallennusVektoriin, jonka MainWindow antoi parametrina kutsuessaan tätä jäsenfunktiota

    for (unsigned int i = 0; i < palaVektori.size(); i += 1) {
        for (unsigned int j = 0; j < palaVektori[i].size(); j += 1) {
            tallennusVektori.push_back(palaVektori[i][j]);
        }
    }

    // TÄSTÄ FUNKTIOSTA EI SAA PALATA ENNEN KUIN KERÄYSVAIHE ON OHI
    // ja: keräysvaihetta ei saa aloittaa ennen kuin viimeinen säie on saapunut Barrierille

}

void Logiikka::muunnaPala(const QImage& kuva, int alkux, int alkuy, int leveys, int korkeus, std::vector<QColor>& vekki) {

    // käsitellään aina suorakaiteen (tai neliön) muotoinen alue
    // alkux, alkuy on alueen vasemman yläkulman koordinaatti kuvan sisällä
    // leveys on käsiteltävän palan leveys ja korkeus sen korkeus

    // esimerkkitapaukesssa esim. aivan ensimmäinen muunnettava pala koko ohjelman
    // suorituksen aikana on kuvan vasen yläkulma: alkux 0, alkuy 0, leveys 100, korkeus 100
    // toisena käsitellään tämän palan alta löytyvä pala: alkux 0, alkuy 100, leveys 100, korkeus 100

    // QT:n koordinaatistossa origo on vasemmassa yläkulmassa

    // silmukassa rivi kerrallaan
    for (int k = 0; k < korkeus; k += 1) {

        // rivin pikselit
        for (int l = 0; l < leveys; l += 1) {

            // muutetaan ensin ylhäältä alas lukien ensimmäisen rivin
            // ensimmäinen eli vasemmanpuolimmaisin pikseli
            // sitten toinen pikseli, kolmas jne.


            // otetaan kuvasta tietyn pikselin kohdalta väri
            QColor vari = kuva.pixelColor(alkux + l, alkuy + k);
            // tehdään mv-konversio: joko otetaan värien keskiarvo TAI otetaan jokaista
            // väriä määrä x oheisten kertoimien osoittamalla tavalla
            // -> molemmat tavat tuottavat mv-kuvan, joista kertoimilla muodostettu
            // on "aidompi"

            //int mv = ( vari.red() + vari.green() + vari.blue() ) / 3;
            int mv = 0.2126*vari.red() + 0.7152*vari.green() + 0.0722*vari.blue();

            vekki.push_back(QColor(mv, mv, mv));
        }
    }
}
